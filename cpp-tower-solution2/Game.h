#pragma once

#include "Stack.h"
#include <vector>

class Game {
  public:
    Game();
    void solve();

    // overloaded <<, allowing us to print Game using cout << game
    friend std::ostream &operator<<(std::ostream &os, const Game &game);

  private:
    std::vector<Stack> stacks_;

  private:
    void _moveCube(Stack &s1, Stack &s2);
    void _move(unsigned start, unsigned end, Stack &source, Stack &target, Stack &spare,
               unsigned depth);
};
