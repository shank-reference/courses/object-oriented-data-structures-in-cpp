#include "Game.h"
#include "uiuc/HSLAPixel.h"
#include <iostream>

using std::cout;
using std::endl;

Game::Game() {
    // create 3 empty stacks
    for (int i = 0; i < 3; i++) {
        Stack stackOfCubes;
        stacks_.push_back(stackOfCubes);
    }

    // create 4 cubes and place them on the 0th stack
    Cube blue(4, uiuc::HSLAPixel::BLUE);
    stacks_[0].push_back(blue);

    Cube orange(3, uiuc::HSLAPixel::ORANGE);
    stacks_[0].push_back(orange);

    Cube purple(2, uiuc::HSLAPixel::PURPLE);
    stacks_[0].push_back(purple);

    Cube yellow(1, uiuc::HSLAPixel::YELLOW);
    stacks_[0].push_back(yellow);
}

void Game::solve() {
    _move(0,                     // start
          stacks_[0].size() - 1, // end
          stacks_[0],            // source stack
          stacks_[2],            // target stack
          stacks_[1],            // spare stack
          0                      // initial dept for printout only
    );
}

std::ostream &operator<<(std::ostream &os, const Game &game) {
    for (unsigned i = 0; i < game.stacks_.size(); i++) {
        os << "Stack[" << i << "]: " << game.stacks_[i];
    }

    return os;
}

void Game::_moveCube(Stack &s1, Stack &s2) {
    Cube cube = s1.removeTop();
    s2.push_back(cube);
}

void Game::_move(unsigned start, unsigned end, Stack &source, Stack &target, Stack &spare,
                 unsigned depth) {
    cout << "Planning (depth=" << depth << "): Move [" << start << ".." << end;
    cout << "] from Stack@" << &source << " -> Stack@" << &target << ", Spare@" << &spare << endl;

    ++depth;

    if (start == end) {
        _moveCube(source, target);
        cout << *this << endl;
    } else {
        _move(start + 1, end, source, spare, target, depth);
        _move(start, start, source, target, spare, depth);
        _move(start + 1, end, spare, target, source, depth);
    }
}
