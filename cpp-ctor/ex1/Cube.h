#pragma once

namespace uiuc {
class Cube {
  public:
    Cube(); // custom default contructor

    double getVolume();
    double getSurfaceArea();
    void setLength(double length);

  private:
    double length_;
};
} // namespace uiuc
