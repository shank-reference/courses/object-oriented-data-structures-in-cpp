#pragma once

namespace uiuc {
class Cube {
  public:
    Cube();              // custom default constructor
    Cube(double length); // one argument constructor

    double getVolume();
    double getSurfaceArea();
    void setLength(double length);

  private:
    double length_;
};
} // namespace uiuc
