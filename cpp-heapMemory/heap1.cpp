#include "Cube.h"
using uiuc::Cube;

int main() {

    int *p = new int;
    Cube *c = new Cube;

    *p = 42;
    (*c).setLength(4);

    delete c;
    delete p;

    return 0;
}
