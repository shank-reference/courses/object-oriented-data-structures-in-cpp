#include "Cube.h"
#include <iostream>
#include <string>

using std::cout;
using std::endl;

using uiuc::Cube;

template <typename T> T my_max(T a, T b) {
    if (a > b) {
        return a;
    }
    return b;
}

int main() {
    cout << "my_max(3, 5): " << my_max(3, 5) << endl;
    cout << "my_max('a', 'b'): " << my_max('a', 'b') << endl;

    cout << "my_max(std::string(\"Hello\"), std::string(\"World\")): "
         << my_max(std::string("Hello"), std::string("World")) << endl;

    cout << "my_max(Cube(3), Cube(6)): " << my_max(Cube(3), Cube(6)) << endl;

    return 0;
}
