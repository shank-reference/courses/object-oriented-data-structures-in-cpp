#include "../Cube.h"
using uiuc::Cube;

#include <iostream>
using std::cout;
using std::endl;

void foo(Cube cube) {
    cout << 3 << endl;
    // Nothing :)
}

int main() {
    cout << 1 << endl;
    Cube c;
    cout << 2 << endl;
    foo(c);
    cout << 4 << endl;

    return 0;
}
