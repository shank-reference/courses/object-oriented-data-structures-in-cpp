#include "../Cube.h"
using uiuc::Cube;

#include <iostream>
using std::cout;
using std::endl;

Cube foo() {
    cout << 2 << endl;
    Cube c;
    cout << 3 << endl;
    return c;
}

int main() {
    cout << 1 << endl;
    Cube c2;
    c2 = foo();
    cout << 4 << endl;

    return 0;
}
