#include "../Cube.h"
using uiuc::Cube;

#include <iostream>
using std::cout;
using std::endl;

int main() {
    cout << 1 << endl;
    Cube c;
    cout << 2 << endl;
    Cube myCube;
    cout << 3 << endl;

    myCube = c;
    cout << 4 << endl;

    return 0;
}
