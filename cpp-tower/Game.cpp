#include "Game.h"
#include "uiuc/HSLAPixel.h"
#include <iostream>

Game::Game() {
    // create 3 empty stacks
    for (int i = 0; i < 3; i++) {
        Stack stackOfCubes;
        stacks_.push_back(stackOfCubes);
    }

    // create 4 cubes and place them on the 0th stack
    Cube blue(4, uiuc::HSLAPixel::BLUE);
    stacks_[0].push_back(blue);

    Cube orange(3, uiuc::HSLAPixel::ORANGE);
    stacks_[0].push_back(orange);

    Cube purple(2, uiuc::HSLAPixel::PURPLE);
    stacks_[0].push_back(purple);

    Cube yellow(1, uiuc::HSLAPixel::YELLOW);
    stacks_[0].push_back(yellow);
}

void Game::solve() {
    // TODO
}

std::ostream &operator<<(std::ostream &os, const Game &game) {
    for (unsigned i = 0; i < game.stacks_.size(); i++) {
        os << "Stack[" << i << "]: " << game.stacks_[i];
    }

    return os;
}
