#include "Stack.h"
#include <iostream>
#include <stdexcept>

using std::cerr;
using std::endl;

void Stack::push_back(const Cube &cube) {
    // Ensure that we do not push a cube on top of a smaller cube
    if (size() > 0 && peekTop().getLength() < cube.getLength()) {
        cerr << "A smaller cube cannot be placed on top of a larger cube." << endl;
        cerr << "  Tried to add Cube(length=" << cube.getLength() << ")" << endl;
        cerr << "  Current Stack: " << *this << endl;

        throw std::runtime_error("A smaller cube cannot be placed on top of a larger one.");
    }

    cubes_.push_back(cube);
}

Cube Stack::removeTop() {
    Cube cube = peekTop();
    cubes_.pop_back();
    return cube;
}

Cube &Stack::peekTop() { return cubes_[cubes_.size() - 1]; }

unsigned Stack::size() const { return cubes_.size(); }

std::ostream &operator<<(std::ostream &os, const Stack &stack) {
    for (unsigned i = 0; i < stack.size(); i++) {
        os << stack.cubes_[i].getLength() << " ";
    }
    os << endl;
    return os;
}
