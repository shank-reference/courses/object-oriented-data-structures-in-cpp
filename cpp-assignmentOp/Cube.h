#pragma once

namespace uiuc {
class Cube {
  public:
    Cube();                // custom default contructor
    Cube(const Cube &obj); // custom copy constructor

    Cube &operator=(const Cube &obj); // custom assignment operator

    double getVolume();
    double getSurfaceArea();
    void setLength(double length);

  private:
    double length_;
};
} // namespace uiuc
