#include <iostream>

#include "Cube.h"
using uiuc::Cube;

using std::cout;
using std::endl;

Cube *CreateUnitCube() {
    Cube c;
    c.setLength(15);
    return &c;
}

int main() {
    Cube *c = CreateUnitCube();
    double volume = c->getVolume();
    double surfaceArea = c->getSurfaceArea();

    cout << "Volume: " << volume << endl;
    cout << "Surface Area: " << surfaceArea << endl;

    return 0;
}
