#include <iostream>

using std::cout;
using std::endl;

int main() {
    int num = 7;

    cout << "Value: " << num << endl;
    cout << "Address: " << &num << endl;

    return 0;
}
