#include "Shape.h"

Shape::Shape() : Shape(1) {
    // Nothing here
}

Shape::Shape(double width) : width_(width) {
    // nothing here either
}

double Shape::getWidth() const { return width_; }
