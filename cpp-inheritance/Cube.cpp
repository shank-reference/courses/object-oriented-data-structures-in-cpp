#include "Cube.h"

namespace uiuc {
Cube::Cube(double width, HSLAPixel color) : Shape(width) { color_ = color; }

double Cube::getVolume() const { return getWidth() * getWidth() * getWidth(); }
} // namespace uiuc
