#include "Cube.h"
#include "HSLAPixel.h"
#include <iostream>

int main() {
    uiuc::Cube c(4, uiuc::HSLAPixel::BLUE);
    std::cout << "Create a BLUE cube! " << std::endl;
    return 0;
}
