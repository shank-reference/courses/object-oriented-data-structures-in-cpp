#pragma once

#include "HSLAPixel.h"
#include "Shape.h"

namespace uiuc {
class Cube : public Shape {
  private:
    HSLAPixel color_;

  public:
    Cube(double width, HSLAPixel color);
    double getVolume() const;
};
} // namespace uiuc
